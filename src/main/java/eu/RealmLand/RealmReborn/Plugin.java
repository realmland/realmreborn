package eu.RealmLand.RealmReborn;

import eu.RealmLand.RealmReborn.services.RealmService;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class Plugin extends JavaPlugin {

    @NotNull
    @Getter
    private RealmService service;

    @Override
    public void onLoad() {
        this.getConfig().options().copyDefaults(true);
        this.saveDefaultConfig();

        service = new RealmService(this);
    }

    @Override
    public void onEnable() {
        try {
            service.initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        try {
            service.terminate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package eu.RealmLand.RealmReborn.logger;

import eu.RealmLand.RealmReborn.statics.TextStatics;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class Logger {

    @Getter
    private List<ChatColor> prefixColors = Arrays.asList(ChatColor.YELLOW);
    @Getter
    private String prefix = "RealmLand Reborn";

    @Getter
    private List<ChatColor> delimiterColors = Arrays.asList(ChatColor.DARK_GRAY);
    @Getter
    private String delimiter = " :: ";

    /**
     * Empty constructor with default values
     */
    public Logger() {
    }

    /**
     * Default constructor
     * @param prefix       Prefix
     * @param prefixColors Prefix's colors
     */
    public Logger(@NotNull String prefix, @NotNull List<ChatColor> prefixColors) {
        this.prefixColors = prefixColors;
        this.prefix = prefix;
    }


    /**
     * Sends debug message to console
     * @param message Message
     */
    public void debug(@NotNull String message) {
        send(ChatColor.GRAY, message, true);
    }

    /**
     * Sends debug message to console with format
     * @param message    Message
     * @param formatVars Format
     */
    public void debug(@NotNull String message, Object ... formatVars) {
        debug(String.format(message, formatVars));
    }


    /**
     * Sends info message to console
     * @param message Message
     */
    public void info(@NotNull String message) {
        send(ChatColor.WHITE, message, true);
    }

    /**
     * Sends info message to console with format
     * @param message    Message
     * @param formatVars Format
     */
    public void info(@NotNull String message, Object ... formatVars) {
        info(String.format(message, formatVars));
    }

    /**
     * Sends error message to console
     * @param message Message
     */
    public void error(@NotNull String message) {
        send(ChatColor.RED, message, true);
    }

    /**
     * Sends error message to console and traces exception
     * @param message Message
     * @param x       Exception
     */
    public void error(@NotNull String message, @NotNull Exception x) {
        send(ChatColor.RED, message, true);
        send(ChatColor.RED, x.toString(), false);
        trace(x);
    }

    /**
     * Sends error message to console with format
     * @param message    Message
     * @param formatVars Format
     */
    public void error(@NotNull String message, Object ... formatVars) {
        error(String.format(message, formatVars));
    }

    /**
     * Sends error message to console with format and trace
     * @param message    Message
     * @param x          Exception
     * @param formatVars Format
     */
    public void error(@NotNull String message, @NotNull Exception x, Object ... formatVars) {
        error(String.format(message, formatVars), x);
    }

    public void trace(@NotNull Exception x) {
        send(ChatColor.RED, "-", false);
        for (StackTraceElement stackTraceElement : x.getStackTrace()) {
            send(ChatColor.WHITE, "§c| §f" + stackTraceElement, false);
        }
        send(ChatColor.RED, "-", false);
    }

    private void send(ChatColor payloadColor, String payload, boolean usePrefix) {
        Bukkit.getConsoleSender().sendMessage((usePrefix ? TextStatics.applyColors(prefix, prefixColors) + TextStatics.applyColors(delimiter, delimiterColors) : "") + payloadColor + payload);
    }
}

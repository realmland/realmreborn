package eu.RealmLand.RealmReborn;

/**
 * Represents plugin service
 */
public interface IService {

    /**
     * Called on service initialization
     * @throws Exception Exception that occurred in Service while initializing
     */
    public void initialize() throws Exception;

    /**
     * Called on service termination
     * @throws Exception Exception that occurred in Service while terminating
     */
    public void terminate()  throws Exception;

    /**
     * Called when service has to be reloaded
     * @throws Exception Exception that occured in Service while reloading
     */
    public default void reload() throws Exception {
        terminate();
        initialize();
    }

}

package eu.RealmLand.RealmReborn.services.api;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import eu.RealmLand.RealmReborn.services.api.model.APIHandler;
import lombok.Getter;
import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.RealmService;
import net.luckperms.api.LuckPerms;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;

public  class APIService implements IService {

    @Getter
    private RealmService realmService;


    @Getter
    private IPApi ipApi;

    @Getter
    private LuckPermsApi luckPermsApi;

    /**
     * Default constructor
     * @param realmService RealmService
     */
    public APIService(RealmService realmService) {
        this.realmService = realmService;
    }

    @Override
    public void initialize() throws Exception {
        this.ipApi = new IPApi();

        this.luckPermsApi = new LuckPermsApi();
       if(luckPermsApi.isAvailable())
           this.realmService.getLogger().info("LuckPermsAPI is available!");
       else
           this.realmService.getLogger().error("LuckPermsAPI is not available!");
    }

    @Override
    public void terminate() throws Exception {

    }

    public class LuckPermsApi extends APIHandler {

        @Getter
        private LuckPerms api = null;

        public LuckPermsApi() {
            RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
            if (provider != null) {
                api = provider.getProvider();
            }
        }

        @Override
        public boolean isAvailable() {
            return api != null;
        }
    }

    public class IPApi extends APIHandler {

        @Getter @NotNull
        private final String STRING_URL = "http://ip-api.com/json/";


        public JsonObject getIpData(@Nullable InetSocketAddress address) {
            if(address == null)
                return null;

            try {
                URL url = new URL(STRING_URL + address.getAddress().getHostAddress());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                StringBuilder response = new StringBuilder();
                int read;
                do {
                    read = connection.getInputStream().read();
                    if (read != -1) {
                        response.append((char) read);
                    }
                } while (read != -1);
                connection.disconnect();

                return  new JsonParser().parse(response.toString()).getAsJsonObject();
            } catch (Exception x) {
                getRealmService().getLogger().error("Failed to read from IPAPI web", x);
            }
            return null;
        }

        @Override
        public boolean isAvailable() {
            try {
                URL url = new URL(STRING_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                if(connection.getInputStream().available() > 0)
                    return true;
                connection.disconnect();
            } catch (Exception x) {
                x.printStackTrace();
            }
            return true;
        }
    }

}

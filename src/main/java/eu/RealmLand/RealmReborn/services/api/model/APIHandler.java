package eu.RealmLand.RealmReborn.services.api.model;

public abstract class APIHandler {
    /**
     * Checks if API is ready to be used
     * @return Boolean
     */
    public abstract boolean isAvailable();

}

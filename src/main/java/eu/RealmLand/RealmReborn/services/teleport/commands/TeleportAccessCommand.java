package eu.RealmLand.RealmReborn.services.teleport.commands;

import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import eu.RealmLand.RealmReborn.services.teleport.TeleportService;
import eu.RealmLand.RealmReborn.tuples.Pair;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TeleportAccessCommand {

    public static class TeleportRequestCall extends Command {

        @NotNull
        private TeleportService teleportService;

        @CommandParameters(name = "tpa", permission = "realmland.player.tpa", permissionDefault = PermissionDefault.TRUE)
        public TeleportRequestCall(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
            this.teleportService = commandService.getRealmService().getTeleportService();
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            if (isPlayer) {
                Player player = (Player) sender;
                if (args.length > 0) {
                    String name = args[0];
                    Player target = Bukkit.getPlayer(name);
                    if (target == null) {
                        sender.sendMessage("§c# §fTento hráč nie je online.");
                        return true;
                    }
                    if(target.getUniqueId().equals(player.getUniqueId())) {
                        target.sendMessage("§c# §fNemôžeš sa portnúť sa seba!");
                        return true;
                    }
                    Profile targetProfile = this.teleportService.getRealmService().getProfileService().getProfile(target.getUniqueId());
                    if (targetProfile != null) {

                        if (targetProfile.getSettings().getBoolean("allow-teleport-requests", true)) {
                            this.teleportService.getTeleportRequests().put(player.getUniqueId(), Pair.of(System.currentTimeMillis(), target.getUniqueId()));
                            sender.sendMessage("§f");
                            sender.sendMessage("§8# §7Žiadosť o teleportáciu zaslaná!");
                            ((CraftPlayer) sender).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("[\"\",{\"text\":\" \\u0020\"},{\"text\":\"§c§lZru\\u0161i\\u0165\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tpacancel\"}}]")));
                            sender.sendMessage("§f");
                            target.sendMessage("§f");
                            ((CraftPlayer) target).getHandle().playerConnection.sendPacket(
                                    new PacketPlayOutChat(
                                            IChatBaseComponent.ChatSerializer
                                                    .a(String.format("[\"\",{\"text\":\"§8# §7Dostal si \\u017eiados\\u0165 o teleportáciu od '§e%s§r'\"}]", player.getName()))));
                            ((CraftPlayer) target).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("[{\"text\":\"  \"},{\"text\":\"§a§lPrija\\u0165\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tpaccept\"}},{\"text\":\"§8,  \"},{\"text\":\"§c§lZamietnu\\u0165\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/tpadeny\"}}]")));
                            target.sendMessage("§f");
                        } else
                            sender.sendMessage("§c# §fHráč je nedostupný.");
                    } else
                        sender.sendMessage("§c# §fTargeted profile not available");
                } else
                    sender.sendMessage("§c# §fŠpecifikuj hráča ku ktorému sa chceš teleportovať.");
            }
            return true;
        }

        @Override
        public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws IllegalArgumentException {
            if (args.length == 1) {
                return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
            }
            return new ArrayList<>();
        }
    }

    public static class TeleportRequestAccept extends Command {
        @NotNull
        private TeleportService teleportService;

        @CommandParameters(name = "tpaccept", permission = "realmland.player.tpa", permissionDefault = PermissionDefault.TRUE)
        public TeleportRequestAccept(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
            this.teleportService = commandService.getRealmService().getTeleportService();
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            if (isPlayer) {
                Player player = (Player) sender;
                String preferred = "";
                Player preferredPlayer = null;
                if (args.length > 0) {
                    preferred = args[0];
                    preferredPlayer = Bukkit.getPlayer(preferred);
                }

                List<UUID> requesters = this.teleportService.getTeleportRequesters(player.getUniqueId());
                if (requesters.size() > 1) {
                    // todo implement more requests
                } else if (requesters.size() == 1) {
                    Player requester = Bukkit.getPlayer(requesters.get(0));
                    if (requester == null) {
                        sender.sendMessage("§c# §fHráč sa odpojil.");
                        this.teleportService.getTeleportRequests().remove(requesters.get(0));
                        return true;
                    }
                    requester.teleport(player);
                    requester.sendMessage("§7Teleportované k '" + player.getName() + "'");
                    this.teleportService.getTeleportRequests().remove(requester.getUniqueId());
                } else
                    sender.sendMessage("§c# §fŽiadne aktívne požiadavky o teleport.");

            }
            return true;
        }

        @Override
        public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws IllegalArgumentException {

            return new ArrayList<>();
        }
    }

    public static class TeleportRequestDeny extends Command {
        @NotNull
        private TeleportService teleportService;

        @CommandParameters(name = "tpadeny", permission = "realmland.player.tpa", permissionDefault = PermissionDefault.TRUE)
        public TeleportRequestDeny(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
            this.teleportService = commandService.getRealmService().getTeleportService();
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            if (isPlayer) {
                Player player = (Player) sender;
                if (args.length > 0) {
                }
                List<UUID> requesters = this.teleportService.getTeleportRequesters(player.getUniqueId());
                if (requesters.size() > 1) {
                    // todo implement more
                } else if (requesters.size() == 1) {
                    UUID requester = requesters.get(0);
                    this.teleportService.getTeleportRequests().remove(requester);
                    Player requesterPlayer = Bukkit.getPlayer(requester);
                    if (requesterPlayer != null)
                        requesterPlayer.sendMessage("§8# §fPožiadavka o teleport ku hráčovi §e" + player.getName() + "§r bola zamietnutá!");
                    player.sendMessage("§8# §fPožiadavka o teleport hráčovi §e" + (requesterPlayer == null ? Bukkit.getOfflinePlayer(requester).getName() : requesterPlayer.getName()) + "§r bola zamietnutá");
                } else
                    sender.sendMessage("§c# §fŽiadne aktívne požiadavky o teleport.");
                return true;

            }
            return true;
        }

    }

    public static class TeleportRequestCancel extends Command {
        @NotNull
        private TeleportService teleportService;

        @CommandParameters(name = "tpacancel", permission = "realmland.player.tpa", permissionDefault = PermissionDefault.TRUE)
        public TeleportRequestCancel(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
            this.teleportService = commandService.getRealmService().getTeleportService();
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            Player player = (Player) sender;

            if (this.teleportService.getTeleportRequests().containsKey(player.getUniqueId())) {
                Pair<Long, UUID> request = this.teleportService.getTeleportRequests().remove(player.getUniqueId());
                player.sendMessage("§8# §fPožiadavka o teleport zrušená.");
                Player target = Bukkit.getPlayer(request.getSecond());
                if(target!=null)
                    target.sendMessage("§8# §fPožiadavka o teleport zrušená hráčom §e" + player.getName());
            } else
                sender.sendMessage("§c# §fŽiadne aktívne požiadavky o teleport.");
            return true;
        }
    }
}

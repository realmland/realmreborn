package eu.RealmLand.RealmReborn.services.teleport.commands;

import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import eu.RealmLand.RealmReborn.services.teleport.TeleportService;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import net.minecraft.server.v1_15_R1.*;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class RandomTeleportCommand extends Command {

    private TeleportService teleportService;

    private Random random;

    @CommandParameters(name = "randomteleport", aliases = {"rtp"}, permission = "realmland.player.rtp", permissionDefault = PermissionDefault.TRUE)
    public RandomTeleportCommand(@NotNull CommandService commandService, @NotNull String name) {
        super(commandService, name);
        teleportService = this.getCommandService().getRealmService().getTeleportService();
        this.random = new Random("Hi! I'm code!".hashCode());
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {

        CompletableFuture<Location> generateSpawn = CompletableFuture.supplyAsync(() -> {
            World world = this.teleportService.getSpawnData().get(this.teleportService.getDefaultSpawnName()).getWorld();
            int tries = 0;
            while (tries < 100) {
                int bound = this.teleportService.getRealmService().getPluginInstance().getConfig().getInt("services.teleport-service.random-teleport-bound", 10000);
                int rX = random.nextInt(bound);
                int rZ = random.nextInt(bound);

                if (random.nextBoolean())
                    rX = Math.negateExact(rX);
                if (random.nextBoolean())
                    rZ = Math.negateExact(rZ);

                int y = world.getHighestBlockYAt(rX, rZ);
                IBlockData acc = ((CraftWorld) world).getHandle().getType(new BlockPosition(rX, y, rZ));
                if (acc.a(TagsBlock.VALID_SPAWN)) {
                    System.out.println("valid");
                    System.out.printf("%d %d %d", rX, y, rZ);
                    return new Location(world, rX, y, rZ);
                }
                tries++;
            }
            return null;
        });
        generateSpawn.whenComplete((loc, e) -> {
            System.out.println("complete");
            Player player = (Player) sender;
            player.teleport(loc);
            sender.sendMessage("y");
            System.out.println("tp");
        });
        sender.sendMessage("s");
        return true;
    }

    @Override
    public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        return null;
    }
}

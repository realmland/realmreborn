package eu.RealmLand.RealmReborn.services.teleport;

import com.google.gson.JsonObject;

import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.tuples.Pair;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.storage.model.JsonStore;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class TeleportService implements IService, Listener {

    @Getter
    @NotNull
    private RealmService realmService;

    @Getter
    @NotNull
    private JsonStore spawnStorage;

    @Getter
    @NotNull
    private String defaultSpawnName = "default";

    // TODO remove option to have more spawns?
    @Getter
    @NotNull
    private final Map<String, Location> spawnData = new HashMap<>();

    @Getter
    @NotNull
    private final Map<UUID, Pair<Long, UUID>> teleportRequests = new HashMap<>();

    @Getter
    @NotNull
    private int requestTimeoutTaskId;

    /**
     * Default constructor
     *
     * @param realmService Instance of RealmService
     */
    public TeleportService(@NotNull RealmService realmService) throws Exception {
        this.realmService = realmService;
        this.spawnStorage = new JsonStore(new File(this.realmService.getStorageService().getStoragePathFile(), "TeleportService/spawn_storage.json"));
        this.realmService.getStorageService().getReloadables().add(this.spawnStorage);
    }

    @Override
    public void initialize() throws Exception {
        spawnStorage.load();
        Bukkit.getPluginManager().registerEvents(this, this.realmService.getPluginInstance());

        try {
            defaultSpawnName = spawnStorage.getJsonData().get("default_spawn_name").getAsString();
        } catch (NullPointerException ignored) {
        }

        JsonObject spawnDataJson = spawnStorage.getJsonData().getAsJsonObject("spawn_data");

        try {
            for (String spawnName : spawnDataJson.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList())) {
                JsonObject singleSpawnData = spawnDataJson.get(spawnName).getAsJsonObject();
                Location location = new Location(
                        Bukkit.getWorld(UUID.fromString(singleSpawnData.get("world").getAsString())),
                        singleSpawnData.get("x").getAsDouble(),
                        singleSpawnData.get("y").getAsDouble(),
                        singleSpawnData.get("z").getAsDouble(),
                        singleSpawnData.get("yaw").getAsFloat(),
                        singleSpawnData.get("pitch").getAsFloat());

                spawnData.put(spawnName, location);
            }
        } catch (NullPointerException ignored) {
        }

        this.requestTimeoutTaskId = Bukkit.getScheduler().runTaskTimerAsynchronously(this.realmService.getPluginInstance(), () -> {
            for (UUID requester : this.getTeleportRequests().keySet()) {
                Pair<Long, UUID> request = this.getTeleportRequests().get(requester);

                Player requesterPlayer = Bukkit.getPlayer(requester);
                Player player = Bukkit.getPlayer(request.getSecond());
                if ((System.currentTimeMillis() - request.getFirst()) >
                        this.getRealmService().getPluginInstance().getConfig().getInt("services.teleport-service.teleport-request-timeout", 40) * 1000) {
                    if (requesterPlayer != null)
                        requesterPlayer.sendMessage("§8# §fHráč §e" + (player == null ? Bukkit.getOfflinePlayer(request.getSecond()).getName() : player.getName()) + "§r nestihol prijať požiadavku o teleport");
                    if (player != null)
                        player.sendMessage("§8# §fNestihol si prijať požiadavku o teleport od hráča §e" + (requesterPlayer == null ? Bukkit.getOfflinePlayer(requester).getName() : requesterPlayer.getName()));
                    this.getTeleportRequests().remove(requester);
                }
            }
        }, 20, 20).getTaskId();


    }

    @Override
    public void terminate() throws Exception {
        Bukkit.getScheduler().cancelTask(this.requestTimeoutTaskId);

        this.spawnStorage.getJsonData().addProperty("default_spawn_name", defaultSpawnName);
        JsonObject spawnDataJson = new JsonObject();
        spawnData.forEach((name, loc) -> {
            JsonObject singleSpawnData = new JsonObject();
            singleSpawnData.addProperty("world", loc.getWorld().getUID().toString());
            singleSpawnData.addProperty("x", loc.getX());
            singleSpawnData.addProperty("y", loc.getY());
            singleSpawnData.addProperty("z", loc.getZ());
            singleSpawnData.addProperty("yaw", loc.getYaw());
            singleSpawnData.addProperty("pitch", loc.getPitch());

            spawnDataJson.add(name, singleSpawnData);
        });
        this.spawnStorage.getJsonData().add("spawn_data", spawnDataJson);

        spawnStorage.save();
    }


    /**
     * Get's players requesting to teleport to specified uuid
     * @param uniqueId UUID
     * @return UUIDs of requesters
     */
    public List<UUID> getTeleportRequesters(@NotNull UUID uniqueId) {
        return this.getTeleportRequests().keySet().stream().map(requester -> {
            Pair<Long, UUID> request = this.getTeleportRequests().get(requester);
            if (request.getSecond().equals(uniqueId))
                return requester;
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Location location = spawnData.getOrDefault(getDefaultSpawnName(), event.getPlayer().getLocation());
        // tp to spawn
        if (!event.getPlayer().hasPlayedBefore()) {
            event.getPlayer().teleport(location);
            this.realmService.getLogger().debug("Teleported player '%s' to spawn", event.getPlayer().getName());
        }
        else
            // force spawn
            if (this.realmService.getPluginInstance().getConfig().getBoolean("services.teleport-service.force-join-spawn", false))
                event.getPlayer().teleport(location);

    }
}

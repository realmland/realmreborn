package eu.RealmLand.RealmReborn.services.teleport.commands;

import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import eu.RealmLand.RealmReborn.services.teleport.TeleportService;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class SpawnCommand {


    public static class TpSpawn extends Command {

        @CommandParameters(name = "spawn", permission = "realmland.player.spawn", permissionDefault = PermissionDefault.TRUE)
        public TpSpawn(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            Player player = (Player) sender;
            TeleportService teleportService = getCommandService().getRealmService().getTeleportService();

            String preferred = teleportService.getDefaultSpawnName();
            if (args.length > 0)
                preferred = args[0];

            Location location = teleportService.getSpawnData().get(preferred);
            if (location == null) {
                sender.sendMessage("§c# §fŠpecifický spawn neexistuje.");
                return true;
            }
            player.teleport(location);
            sender.sendMessage("§a# §fTeleportované.");
            return true;

        }
    }

    public static class SetSpawn extends Command {

        @CommandParameters(name = "setspawn", permission = "realmland.admin.spawn")
        public SetSpawn(@NotNull CommandService commandService, @NotNull String name) {
            super(commandService, name);
        }

        @Override
        public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
            Player player = (Player) sender;
            TeleportService teleportService = getCommandService().getRealmService().getTeleportService();

            String name = teleportService.getDefaultSpawnName();
            if (args.length > 0)
                name = args[0];
            teleportService.getSpawnData().put(name, player.getLocation());
            sender.sendMessage("§a# §fSpawn '" + name + "' nastavený.");
            return true;
        }
    }

}

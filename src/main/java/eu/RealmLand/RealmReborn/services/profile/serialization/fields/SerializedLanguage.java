package eu.RealmLand.RealmReborn.services.profile.serialization.fields;

import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import eu.RealmLand.RealmReborn.services.i18n.Language;
import org.jetbrains.annotations.NotNull;

public class SerializedLanguage extends SerializedField<Language> {

    @Override
    public Language deserialize(@NotNull String raw) {
        return Language.fromStoreName(raw);
    }

}

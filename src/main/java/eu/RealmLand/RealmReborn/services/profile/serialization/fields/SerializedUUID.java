package eu.RealmLand.RealmReborn.services.profile.serialization.fields;

import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class SerializedUUID extends SerializedField<UUID> {

    @Override
    public UUID deserialize(@NotNull String raw) {
        return UUID.fromString(raw);
    }

}

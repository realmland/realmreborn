package eu.RealmLand.RealmReborn.services.profile.serialization.model;

import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Serialize {

    String key();
    Class<? extends SerializedField<?>> valueType();
    boolean specialized() default false;

}

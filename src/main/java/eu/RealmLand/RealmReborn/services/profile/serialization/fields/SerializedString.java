package eu.RealmLand.RealmReborn.services.profile.serialization.fields;

import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import org.jetbrains.annotations.NotNull;

public class SerializedString extends SerializedField<String> {

    @Override
    public String deserialize(@NotNull String raw) {
        return raw;
    }
}

package eu.RealmLand.RealmReborn.services.profile.converter;

import eu.RealmLand.RealmReborn.services.profile.converter.model.ProfileVersion;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import org.jetbrains.annotations.NotNull;

public class ProfileModelConverter {

    @NotNull
    private RealmService realmService;

    private final String PROFILE_VERSION = Profile.class.getAnnotation(ProfileVersion.class).value();

    public ProfileModelConverter() {

    }
}

package eu.RealmLand.RealmReborn.services.profile.serialization.fields;

import com.google.gson.JsonParser;
import eu.RealmLand.RealmReborn.services.profile.model.ProfileSettings;
import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import org.jetbrains.annotations.NotNull;

public class SerializedProfileSettings extends SerializedField<ProfileSettings> {

    @Override
    public @NotNull String serialize(@NotNull Object object) {
        return ((ProfileSettings) object).saveToJson().toString();
    }

    @Override
    public ProfileSettings deserialize(@NotNull String raw) {
        ProfileSettings settings = new ProfileSettings();
        settings.loadFromJson(new JsonParser().parse(raw).getAsJsonObject());
        return settings;
    }
}

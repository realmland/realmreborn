package eu.RealmLand.RealmReborn.services.profile.model;

import eu.RealmLand.RealmReborn.services.i18n.Language;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * Profile builder
 */
public class ProfileBuilder {

    @NotNull
    private Profile toBuild;

    /**
     * Default constructor
     * @param uuid UUID of profile
     * @param name NAME of profile
     */
    private ProfileBuilder(@NotNull UUID uuid, @NotNull String name) {
        this.toBuild = new Profile();
        this.toBuild.setUuid(uuid);
        this.toBuild.setName(name);
    }

    /**
     * Creates new instance of ProfileBuilder
     * @param player Player instance
     * @return ProfileBuilder
     */
    public static ProfileBuilder forPlayer(@NotNull Player player) {
        return new ProfileBuilder(player.getUniqueId(), player.getName());
    }

    /**
     * Creates new instance of ProfileBuilder
     * @param uuid UUID for profile
     * @param name Name for profile
     * @return Profile Builder
     */
    public static ProfileBuilder forPlayer(@NotNull UUID uuid, @NotNull String name) {
        return new ProfileBuilder(uuid, name);
    }

    /**
     * Sets profile language
     * @param language Profile language
     * @return Profile Builder
     */
    public ProfileBuilder withLanguage(@NotNull Language language) {
        this.toBuild.setLanguage(language);
        return this;
    }

    /**
     * Sets profile discord
     * @param discord Discord
     * @return Profile builder
     */
    public ProfileBuilder withDiscord(@NotNull String discord) {
        this.toBuild.setDiscord(discord);
        return this;
    }

    /**
     * Sets profile ISP
     * @param isp ISP
     * @return Profile Builder
     */
    public ProfileBuilder withISP(@NotNull String isp) {
        this.toBuild.setIsp(isp);
        return this;
    }

    /**
     * Sets profile Country
     * @param country Country
     * @return Profile Builder
     */
    public ProfileBuilder withCountry(@NotNull String country) {
        this.toBuild.setCountry(country);
        return this;
    }

    /**
     * Sets profile Settings
     * @param settings Settings
     * @return Profile Builder
     */
    public ProfileBuilder withSettings(@NotNull ProfileSettings settings) {
        this.toBuild.setSettings(settings);
        return this;
    }

    public Profile build() {
        return toBuild;
    }

}

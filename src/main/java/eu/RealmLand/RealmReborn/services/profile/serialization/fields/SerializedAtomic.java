package eu.RealmLand.RealmReborn.services.profile.serialization.fields;

import com.google.common.util.concurrent.AtomicDouble;
import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Serialized atomic numbers
 */
public class SerializedAtomic {
    /**
     * Atomic double
     */
    public static class Double extends SerializedField<AtomicDouble> {

        @Override
        public AtomicDouble deserialize(@NotNull String raw) {
            try {
                return new AtomicDouble(NumberFormat.getCurrencyInstance(Locale.ENGLISH).parse(raw).doubleValue());
            } catch (ParseException e) {
                return new AtomicDouble(-1);
            }
        }

        @Override
        public @NotNull String serialize(@NotNull Object object) {
            return String.format("%f", ((AtomicDouble) object).get());
        }

    }

    /**
     * Atomic integer
     */
    public static class Integer extends SerializedField<AtomicInteger> {

        @Override
        public AtomicInteger deserialize(@NotNull String raw) {
            try {
                return new AtomicInteger(NumberFormat.getCurrencyInstance(Locale.ENGLISH).parse(raw).intValue());
            } catch (ParseException e) {
                return new AtomicInteger(-1);
            }
        }

        @Override
        public @NotNull String serialize(@NotNull Object object) {
            return String.format("%d", ((AtomicInteger)object).get());
        }

    }
}

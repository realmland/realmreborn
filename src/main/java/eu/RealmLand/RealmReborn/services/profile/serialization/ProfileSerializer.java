package eu.RealmLand.RealmReborn.services.profile.serialization;

import com.google.gson.JsonObject;
import eu.RealmLand.RealmReborn.services.profile.serialization.fields.model.SerializedField;
import eu.RealmLand.RealmReborn.services.profile.serialization.model.Serialize;
import eu.RealmLand.RealmReborn.tuples.Pair;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import eu.RealmLand.RealmReborn.services.storage.model.JsonStore;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProfileSerializer {

    @NotNull
    private RealmService realmService;

    @NotNull
    @Getter
    private final Map<String, Pair<String, Serialize>> profileFieldMap = new HashMap<>();

    /**
     * Default constructor
     *
     * @param realmService Instance to realm service
     */
    public ProfileSerializer(@NotNull RealmService realmService) {
        this.realmService = realmService;
        for (Field declaredField : Profile.class.getDeclaredFields()) {
            Serialize serializeData = declaredField.getAnnotation(Serialize.class);
            if (serializeData != null)
                this.profileFieldMap.put(serializeData.key(), Pair.of(declaredField.getName(), serializeData));
        }
        this.realmService.getLogger().info("Serializing %d fields for profile", profileFieldMap.size());
    }

    /**
     * Loads profile from storage
     *
     * @param id UUID of player
     * @return Profile
     */
    public synchronized Profile loadFromStorage(@NotNull UUID id) {
        JsonStore storage = getProfileStoreFile(id);
        if (!isProfileStored(id)) {
            this.realmService.getLogger().error("Serialized profile file for player '%s' is not stored!", Bukkit.getOfflinePlayer(id).getName());
            return null;
        }

        try {
            storage.load();
        } catch (Exception e) {
            this.realmService.getLogger().error("Failed to load serialized profile json file for '%s'!", e, Bukkit.getOfflinePlayer(id).getName());
            return null;
        }

        JsonObject jsonData = storage.getJsonData();

        Profile profile = new Profile();
        this.profileFieldMap.forEach((key, fieldData) -> {
            Serialize serializeData = fieldData.getSecond();
            String fieldName = fieldData.getFirst();

            try {
                Field liveField = profile.getClass().getDeclaredField(fieldName);
                liveField.setAccessible(true);
                String value = jsonData.get(key).getAsString();
                liveField.set(profile, serializeData.valueType().getConstructor().newInstance().deserialize(value));
            } catch (NoSuchFieldException | NoSuchMethodException e) {
                this.realmService.getLogger().error("Failed to find field named %s in profile class", e, fieldName);
            } catch (IllegalAccessException e) {
                this.realmService.getLogger().error("Failed to access field named %s in profile class", e, fieldName);
            } catch (InstantiationException | InvocationTargetException e) {
                this.realmService.getLogger().error("Failed to access constructor for SerializedField named %s", e, serializeData.valueType().getSimpleName());
            } catch (Exception x) {
                this.realmService.getLogger().error("Serialized profile for '%s' is corrupted! Missing key %s", x, Bukkit.getOfflinePlayer(id).getName(), key);
            }

        });
        return profile;


    }


    /**
     * Saves profile to storage
     *
     * @param profile Profile to save
     * @return Boolean
     */
    public synchronized boolean saveToStorage(@NotNull Profile profile) {
        JsonStore storage = getProfileStoreFile(profile.getUuid());
        if (isProfileStored(profile.getUuid())) {
            try {
                storage.delete();
            } catch (Exception e) {
                this.realmService.getLogger().error("Failed to delete profile file for '%s'!", e, profile.getName());
                return false;
            }
        }
        JsonObject jsonData = new JsonObject();
        this.profileFieldMap.forEach((key, fieldData) -> {
            Serialize serializeData = fieldData.getSecond();
            String fieldName = fieldData.getFirst();

            try {
                Field field = profile.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);

                SerializedField<?> serializedField = serializeData.valueType().getConstructor().newInstance();
                jsonData.addProperty(key, serializedField.serialize(field.get(profile)));

            } catch (NoSuchFieldException | NoSuchMethodException e) {
                this.realmService.getLogger().error("Failed to find field named %s in profile class", e, fieldName);
            } catch (IllegalAccessException e) {
                this.realmService.getLogger().error("Failed to access field named %s in profile class", e, fieldName);
            } catch (InstantiationException | InvocationTargetException e) {
                this.realmService.getLogger().error("Failed to access constructor for SerializedField named %s", e, serializeData.valueType().getSimpleName());
            }
        });
        try {
            storage.create(jsonData);
        } catch (Exception e) {
            this.realmService.getLogger().error("Failed to save profile file for '%s'", e, profile.getName());
            this.realmService.getLogger().debug(jsonData.toString());
            return false;
        }
        return true;
    }

    /**
     * Checks if profile is stored
     *
     * @param uuid UUID of profile
     * @return Boolean
     */
    public boolean isProfileStored(@NotNull UUID uuid) {
        return getProfileStoreFile(uuid).isAvailable();
    }


    public JsonStore getProfileStoreFile(@NotNull UUID id) {
        return new JsonStore(new File(this.realmService.getStorageService().getStoragePathFile(), "ProfileService/profiles/" + id.toString() + ".json"));
    }

}

package eu.RealmLand.RealmReborn.services.profile.serialization.fields.model;

import org.jetbrains.annotations.NotNull;

public abstract class SerializedField<T> {

    public @NotNull String serialize(@NotNull Object object) {
        return object.toString();
    }

    public abstract T deserialize(@NotNull String raw);

    public <N> N safeCast(Object object, N newClass) {
        if(newClass.getClass().isAssignableFrom(object.getClass()))
            return (N) object;
        return null;
    }
}

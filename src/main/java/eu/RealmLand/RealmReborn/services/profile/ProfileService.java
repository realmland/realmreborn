package eu.RealmLand.RealmReborn.services.profile;

import com.google.gson.JsonObject;
import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import eu.RealmLand.RealmReborn.services.profile.serialization.ProfileSerializer;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.i18n.Language;
import eu.RealmLand.RealmReborn.services.profile.model.ProfileBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProfileService implements IService, Listener {

    @Getter
    @NotNull
    private RealmService realmService;

    @Getter
    @NotNull
    private ProfileSerializer profileSerializer;

    @Getter
    @NotNull
    private final Map<UUID, Profile> loadedProfiles = new HashMap<>();


    /**
     * Default constructor
     *
     * @param realmService Realm Service instance
     */
    public ProfileService(@NotNull RealmService realmService) {
        this.realmService = realmService;
        this.profileSerializer = new ProfileSerializer(realmService);
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, this.realmService.getPluginInstance());
        // load all
    }

    @Override
    public void terminate() throws Exception {
        // save all
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (this.loadedProfiles.containsKey(event.getPlayer().getUniqueId()))
            event.getPlayer().kickPlayer("§cProfile with this UUID already loaded!");

        if (this.profileSerializer.isProfileStored(event.getPlayer().getUniqueId())) {
            long nanoStart = System.nanoTime();
            Profile profile = this.profileSerializer.loadFromStorage(event.getPlayer().getUniqueId());
            this.loadedProfiles.put(event.getPlayer().getUniqueId(), profile);
            long nanoStop = System.nanoTime();

            if (profile != null)
                this.realmService.getLogger().info("Loaded profile '%s' in %.3fms", event.getPlayer().getName(), (nanoStop - nanoStart) / 1e+6d);
            else
                this.realmService.getLogger().error("Failed to load profile for player '%s' because file doesn't exist even tho it is stored?", event.getPlayer().getName());
        } else {
            long nanoStart = System.nanoTime();

            Language language = null;

            JsonObject profileData = this.realmService.getApiService().getIpApi().getIpData(event.getPlayer().getAddress());
            if (profileData.get("status").getAsString().equalsIgnoreCase("fail")) {
                getRealmService().getLogger().error("Response from IpAPI marked as failure!");
            } else {
                language = Language.fromStoreName(profileData.get("countryCode").getAsString().toUpperCase());
            }
            if (language == null)
                language = Language.EN;

            String isp = profileData.get("isp").getAsString();
            String country = profileData.get("country").getAsString();

            Profile profile = ProfileBuilder
                    .forPlayer(event.getPlayer())
                    .withLanguage(language)
                    .withCountry(country)
                    .withISP(isp)
                    .build();

            long nanoStop = System.nanoTime();
            this.realmService.getLogger().info("Created profile for '%s' in %.3fms", event.getPlayer().getName(), (nanoStop - nanoStart) / 1e+6d);
            System.out.println(profile);
            this.loadedProfiles.put(event.getPlayer().getUniqueId(), profile);
        }

    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Profile profile = getProfile(event.getPlayer());
        if (profile != null) {
            long nanoStart = System.nanoTime();
            this.profileSerializer.saveToStorage(profile);
            this.loadedProfiles.remove(event.getPlayer().getUniqueId());
            this.realmService.getLogger().info("Saved profile '%s' in %.3fms", event.getPlayer().getName(), (System.nanoTime() - nanoStart) / 1e+6d);
        } else
            this.realmService.getLogger().error("Failed to save profile for player '%s' because it's not loaded", event.getPlayer().getName());

    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        System.out.println("called");
    }

    /**
     * Gets loaded profile, if profile is not loaded,
     * @param uuid
     * @return
     */
    public Profile getProfile(@NotNull UUID uuid) {
        Profile profile = loadedProfiles.get(uuid);
        return profile == null ? getOfflineProfile(uuid) : profile;
    }

    public Profile getProfile(@NotNull Player player) {
        return getProfile(player.getUniqueId());
    }

    public Profile getOfflineProfile(@NotNull UUID uuid) {
        return this.profileSerializer.loadFromStorage(uuid);
    }

}

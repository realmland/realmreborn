package eu.RealmLand.RealmReborn.services.profile.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class ProfileSettings {

    @Getter @NotNull
    private final Map<String, JsonPrimitive> settings = new HashMap<>();


    public String getString(@NotNull String key) {
        try {
            return settings.get(key).getAsString();
        } catch (Exception x) {
            return null;
        }
    }

    public String getString(@NotNull String key, @NotNull String def) {
        try {
            return settings.getOrDefault(key, new JsonPrimitive(def)).getAsString();
        } catch (Exception x) {
            return null;
        }
    }

    public boolean getBoolean(@NotNull String key) {
        try {
            return settings.get(key).getAsBoolean();
        } catch (Exception x) {
            return false;
        }
    }

    public boolean getBoolean(@NotNull String key, boolean def) {
        try {
            return settings.getOrDefault(key, new JsonPrimitive(def)).getAsBoolean();
        } catch (Exception x) {
            return false;
        }
    }


    /**
     * Loads Settings from JsonObject
     * @param jsonObject JsonObject
     */
    public void loadFromJson(@NotNull JsonObject jsonObject) {
        jsonObject.entrySet().forEach(entry -> {
            this.settings.put(entry.getKey(), entry.getValue().getAsJsonPrimitive());
        });
    }

    /**
     * Saves Settings to JsonObject
     * @return JsonObject
     */
    public @NotNull JsonObject saveToJson() {
        JsonObject jsonObject = new JsonObject();
        this.settings.forEach(jsonObject::add);
        return jsonObject;
    }
}

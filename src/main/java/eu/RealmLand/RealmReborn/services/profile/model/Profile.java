package eu.RealmLand.RealmReborn.services.profile.model;

import com.google.common.util.concurrent.AtomicDouble;
import eu.RealmLand.RealmReborn.services.profile.converter.model.ProfileVersion;
import eu.RealmLand.RealmReborn.services.profile.serialization.fields.*;
import eu.RealmLand.RealmReborn.services.profile.serialization.model.Serialize;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import eu.RealmLand.RealmReborn.services.i18n.Language;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@ProfileVersion("1.0")
public class Profile {

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @NotNull
    @Serialize(key = "profile_name", valueType = SerializedString.class)
    private String name;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @NotNull
    @Serialize(key = "profile_uuid", valueType = SerializedUUID.class)
    private UUID uuid;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @NotNull
    @Serialize(key = "profile_country", valueType = SerializedString.class)
    private String country = "";

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @NotNull
    @Serialize(key = "profile_isp", valueType = SerializedString.class)
    private String isp = "";

    @Getter
    @Setter()
    @NotNull
    @Serialize(key = "profile_discord", valueType = SerializedString.class)
    private String discord = "";

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @Serialize(key = "profile_coins", valueType = SerializedAtomic.Double.class)
    private AtomicDouble coins = new AtomicDouble(0);

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @Serialize(key = "profile_tokens", valueType = SerializedAtomic.Double.class)
    private AtomicDouble tokens = new AtomicDouble(0);

    @Getter
    @Setter()
    @NotNull
    @Serialize(key = "profile_language", valueType = SerializedLanguage.class)
    private Language language = Language.EN;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private boolean offline = false;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    @NotNull
    @Serialize(key = "profile_settings", valueType = SerializedProfileSettings.class)
    private ProfileSettings settings = new ProfileSettings();

    /**
     * Deposits specified amount of coins to profile
     * @param amount Amount of coins to deposit.
     * @return New balance
     */
    public double depositCoins(double amount) {
        return this.coins.addAndGet(amount);
    }

    /**
     * Deposits specified amount from coins to profile
     * @param amount Amount of coins to withdraw. <strong color="#ff4040">Can not be negative!</strong>
     * @return New balance
     */
    public double withdrawCoins(double amount) {
        return this.coins.addAndGet(-amount);
    }

    /**
     * Deposits specified amount of tokens to profile
     * @param amount Amount of tokens to deposit.
     * @return New balance
     */
    public double depositTokens(double amount) {
        return this.tokens.addAndGet(amount);
    }

    /**
     * Withdraws specified amount of tokens from profile
     * @param amount Amount of tokens to withdraw. <strong color="#ff4040">Can not be negative!</strong>
     * @return New balance
     */
    public double withdrawTokens(double amount) {
        return this.tokens.addAndGet(-amount);
    }


    /**
     * Check if profile has enough coins
     * @param value Amount of coins
     * @return bool true if has enough else false
     */
    public boolean hasEnoughCoins(double value) {
        return  value <= coins.get();
    }

    /**
     * Check if profile has enough tokens
     * @param value Amount of tokens
     * @return bool true if has enough else false
     */
    public boolean hasEnoughTokens(int value) {
        return  value <= tokens.get();
    }
}

package eu.RealmLand.RealmReborn.services.i18n.store;

import eu.RealmLand.RealmReborn.services.i18n.Language;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public abstract class Translation {

    public static class Entry {
        @Getter
        private String prefix = "";

        @Getter
        private Map<Language, String> translated = new HashMap<>();

        /**
         * Default constructor
         * @param translated Translated messages
         */
        public Entry(Map<Language, String> translated) {
            this.translated = translated;
        }

        /**
         * Gets translated message with prefix
         * @param language Language
         * @return Message
         */
        public @Nullable String get(@NotNull Language language) {
            return prefix + getRaw(language);
        }

        /**
         * Gets translated message <strong>without< the prefix/strong>
         * @param language Language
         * @return Message
         */
        public @Nullable String getRaw(@NotNull Language language) {
            return translated.get(language);
        }
    }

    public static class Category {

        @Getter
        private final String name;

        @Getter
        private Map<String, Map<String, Translation.Entry>> translations = new HashMap<>();

        /**
         * Default constructor
         * @param name Name of category
         */
        public Category(@NotNull String name) {
            this.name = name;
        }

        public Translation.Entry get(@NotNull String categoryKey) {
            return null;
        }
    }



}

package eu.RealmLand.RealmReborn.services.i18n.store;

import eu.RealmLand.RealmReborn.services.i18n.LocalisationService;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranslationSource {

    @Getter
    private LocalisationService localisationService;

    @Getter
    private final List<String> categories = Arrays.asList("infos", "errors");

    /**
     * Stores all categories and their translations
     */
    private Map<String, Map<String, Translation.Category>> translations = new HashMap<>();

    /**
     * Default constructor
     *
     * @param localisationService Instance to localisation service
     */
    public TranslationSource(@NotNull LocalisationService localisationService) {
        this.localisationService = localisationService;
    }

    /**
     * Loads source from file
     *
     * @param file File
     */
    public void loadFromFile(@NotNull File file) {
        try (FileReader reader = new FileReader(file)) {
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

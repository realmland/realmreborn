package eu.RealmLand.RealmReborn.services.i18n;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * Language
 */
public enum Language {

    SK("sk"), CZ("cz"), EN("en");

    @Getter
    private String storeName;

    Language(String storeName) {
        this.storeName = storeName;
    }

    public static Language fromStoreName(@NotNull String name) {
        for(Language language : values()) {
            if(language.storeName.equalsIgnoreCase(name))
                return language;
        }
        return null;
    }

    @Override
    public String toString() {
        return storeName;
    }
}

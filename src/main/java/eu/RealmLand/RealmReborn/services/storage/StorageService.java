package eu.RealmLand.RealmReborn.services.storage;

import eu.RealmLand.RealmReborn.IService;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.storage.model.IStore;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StorageService implements IService {

    @Getter
    @NotNull
    private RealmService realmService;

    @NotNull
    private File storagePath;

    @Getter
    private List<IStore> reloadables = new ArrayList<>();

    /**
     * Default constructor
     *
     * @param realmService Instance of RealmService
     */
    public StorageService(@NotNull RealmService realmService) {
        this.realmService = realmService;
        this.storagePath = new File(this.realmService.getPluginInstance().getDataFolder().getAbsolutePath() + "/storage");
        if (this.storagePath.mkdirs())
            this.realmService.getLogger().info("Made storage path.");
    }

    @Override
    public void initialize() throws Exception {

    }

    @Override
    public void terminate() throws Exception {

    }

    @Override
    public void reload() throws Exception {
        reloadables.stream()
                .map(iStore -> {
                    try {
                        iStore.save();
                        return iStore;
                    } catch (Exception e) {
                        this.realmService.getLogger().error("Failed to save %s", e, iStore.getSource().getName());
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .forEach(iStore -> {
                    try {
                        iStore.load();
                    } catch (Exception e) {
                        this.realmService.getLogger().error("Failed to load %s", e, iStore.getSource().getName());
                    }
                });
    }

    /**
     * Gets path to storage folder
     *
     * @return Path string
     */
    public String getStoragePath() {
        return this.storagePath.getAbsolutePath();
    }

    /**
     * Gets path to storage folder as File
     *
     * @return File
     */
    public File getStoragePathFile() {
        return this.storagePath;
    }

}

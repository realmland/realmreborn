package eu.RealmLand.RealmReborn.services.storage.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;

public class JsonStore implements IStore {

    @Getter
    @NotNull
    private File source;

    @Getter
    @Setter
    @NotNull
    private JsonObject jsonData = null;

    /**
     * Default constructor
     * @param source File
     */
    public JsonStore(@NotNull File source) {
        this.source = source;
    }

    /**
     * Constructor
     * @param path Path to file
     */
    public JsonStore(@NotNull String path) {
        this(new File(path));
    }


    @Override
    public void load() throws Exception {
        if(!source.exists())
            create((InputStream) null);
        if(source.isDirectory())
            throw new IllegalAccessException("File can not be directory!");

        try(FileReader reader = new FileReader(source)) {
            jsonData = new JsonParser().parse(reader).getAsJsonObject();
        }
    }

    @Override
    public void save() throws Exception {
        if(source.exists())
            delete();
        create((InputStream) null);

        if(source.isDirectory())
            throw new IllegalAccessException("File can not be directory!");

        try (FileWriter writer = new FileWriter(source)) {
            writer.write(jsonData.toString());
        }
    }

    @Override
    public boolean create(@Nullable InputStream defaultSource) throws Exception {
        source.getParentFile().mkdirs();
        if(!source.exists())
            if(!source.createNewFile())
                return false;

        try(FileWriter writer = new FileWriter(source)) {
            if(defaultSource != null) {
                char read = 0x0;
                do {
                    read = (char)defaultSource.read();
                    if(read > 0x0)
                        writer.write(read);
                } while (read > 0x0);
            } else
                writer.write("{}");
            return true;
        }
    }

    public <T extends JsonElement> boolean create(@NotNull T data) throws Exception {
        if (!source.exists()) {
            source.getParentFile().mkdirs();
            if (!source.createNewFile())
                return false;
        }

        try (FileWriter writer = new FileWriter(source)) {
            writer.write(data.toString());
            return true;
        }
    }

    @Override
    public boolean delete() throws Exception {
        if(source.exists())
            return source.delete();
        return false;
    }

    @Override
    public boolean isAvailable() {
        return source.exists();
    }
}

package eu.RealmLand.RealmReborn.services.storage.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.InputStream;

public interface IStore {

    void load() throws Exception;
    void save() throws Exception;

    boolean create(@Nullable InputStream defaultSource) throws Exception;
    boolean delete() throws Exception;

    boolean isAvailable();

    File getSource();

}

package eu.RealmLand.RealmReborn.services.storage.model;

import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.InputStream;

public class YamlStore implements IStore {

    @Getter @NotNull
    private File source;

    @Getter @NotNull
    private YamlConfiguration yamlConfiguration;


    @Override
    public void load() throws Exception {

    }

    @Override
    public void save() throws Exception {

    }

    @Override
    public boolean create(@Nullable InputStream defaultSource) throws Exception {
        return false;
    }

    @Override
    public boolean delete() throws Exception {
        return false;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }
}

package eu.RealmLand.RealmReborn.services.chat;


import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.RealmService;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;
import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;
import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;

public class ChatService implements IService, Listener {

    private RealmService realmService;

    /**
     * Default constructor
     * @param realmService Instance of RealmService
     */
    public ChatService(RealmService realmService) {
        this.realmService = realmService;
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, realmService.getPluginInstance());
    }

    @Override
    public void terminate() throws Exception {

    }

    @Override
    public void reload() throws Exception {

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player eventPlayer = event.getPlayer();
        UUID uuid = eventPlayer.getUniqueId();

        String playerPrefix = "";
        String playerNick   = eventPlayer.getName();
        if(eventPlayer.isOp() || eventPlayer.hasPermission("realmland.staff"))
            playerNick = this.realmService.getPluginInstance().getConfig().getString("services.chat-service.staff-nick-color", "§7") + playerNick;

        LuckPerms api = this.realmService.getApiService().getLuckPermsApi().getApi();
        if(api != null) {
            User eventUser = api.getUserManager().getUser(uuid);
            if (eventUser != null) {
                Optional<QueryOptions> options = api.getContextManager().getQueryOptions(eventUser);
                if (options.isPresent())
                    playerPrefix = eventUser.getCachedData().getMetaData(options.get()).getPrefix();
            }
        }
        if(playerPrefix == null)
            playerPrefix = "N/A";
        playerPrefix = ChatColor.translateAlternateColorCodes('&', playerPrefix);

        String message = StringEscapeUtils.escapeJava(event.getMessage());
        String chatFormat = this.realmService.getPluginInstance().getConfig().getString("services.chat-service.chat-format");

        if(chatFormat == null || chatFormat.isEmpty())
            return;

        message = ChatColor.translateAlternateColorCodes('&', message);
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(message.toLowerCase().contains("@" + player.getName().toLowerCase())) {
                message = message.replaceAll("(?i)@" + player.getName(), "§e@" + StringEscapeUtils.escapeJava(player.getName()) + "§r");
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1f, 1f);
            }
        }

        chatFormat = chatFormat.replaceAll("(?i)%player_name%", playerNick)
                .replaceAll("(?i)%player_message%", Matcher.quoteReplacement(message))
        .replaceAll("(?i)%player_group%", playerPrefix);


        try {
            PacketPlayOutChat packet = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(chatFormat));
            System.out.printf("%s §7%s §8-§f %s\n", playerPrefix, playerNick, message);
            Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player)).forEach(player -> player.getHandle().playerConnection.sendPacket(packet));

        } catch (Exception x) {
            x.printStackTrace();
        }
        finally {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage("");
        if(event.getPlayer().hasPlayedBefore())
            Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player)).forEach(player -> player.sendMessage("§a# §fHráč §e" + event.getPlayer().getName() + "§f sa pripojil."));
        else
            Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player)).peek(player -> player.sendMessage("§a# §fHráč §e" + event.getPlayer().getName() + "§f sa pripojil poprvýkrat.")).forEach(player -> player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_CHIME, 1f, 1f));
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage("");
        Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player)).forEach(player -> player.sendMessage("§c# §fHráč §e" + event.getPlayer().getName() + "§f sa odpojil."));
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player)).forEach(player -> player.sendMessage("§8# §7" + event.getDeathMessage()));
        event.setDeathMessage("");
    }
}

package eu.RealmLand.RealmReborn.services.tab;

import eu.RealmLand.RealmReborn.statics.TextStatics;
import lombok.Getter;
import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.statics.ScoreboardStatics;
import me.clip.placeholderapi.PlaceholderAPI;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class TabService implements IService, Listener {

    private RealmService realmService;

    private List<String> headerText = new ArrayList<>();
    private List<String> footerText = new ArrayList<>();

    @Getter
    private AtomicBoolean reloadRequired = new AtomicBoolean(false);

    @Getter
    private Scoreboard sortingBoard;

    /**
     * Refresh rate in TICKS
     */
    @Getter
    private int refreshRate;

    @Getter
    private int taskId;

    /**
     * Default constructor
     *
     * @param realmService Instance of RealmService
     */
    public TabService(RealmService realmService) {
        this.realmService = realmService;
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, this.realmService.getPluginInstance());
        loadSettings();
        makeSortingBoard();
        taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(this.realmService.getPluginInstance(), this::refresh, refreshRate, refreshRate).getTaskId();

    }

    @Override
    public void terminate() throws Exception {
        Bukkit.getScheduler().cancelTask(taskId);
    }


    /**
     * Loads settings from config
     */
    public void loadSettings() {
        headerText = this.realmService.getPluginInstance().getConfig().getStringList("services.tab-service.header-text");
        footerText = this.realmService.getPluginInstance().getConfig().getStringList("services.tab-service.footer-text");
        refreshRate = this.realmService.getPluginInstance().getConfig().getInt("services.tab-service.tick-refresh-rate", 10);
    }

    public void makeSortingBoard() {
        sortingBoard = Bukkit.getScoreboardManager().getNewScoreboard();
        LuckPerms api = this.realmService.getApiService().getLuckPermsApi().getApi();
        if (api != null) {
            api.getGroupManager().getLoadedGroups().stream().forEach(group -> {
                AtomicInteger weight = new AtomicInteger(this.realmService.getPluginInstance().getConfig().getInt("services.tab-service.default-weight", 0));
                group.getWeight().ifPresent(weight::set);

                String priority = ScoreboardStatics.weightToAlpha(weight.get());
                Team team = sortingBoard.registerNewTeam(priority + group.getName());
                Optional<Node> data = group.getNodes().stream().filter(node -> node.getType().equals(NodeType.PREFIX)).findFirst();
                String prefix = "§8N/A";
                if (data.isPresent())
                    prefix = data.get().getKey().replaceFirst("prefix.[0-9].", "");

                team.setPrefix(" " + ChatColor.translateAlternateColorCodes('&', prefix) + " ");
            });
        } else
            this.realmService.getLogger().error("Couldn't use LuckPerms API to load groups!");
    }


    /**
     * Refreshes TAB
     */
    public void refresh() {
        Bukkit.getOnlinePlayers().stream().map(player -> (CraftPlayer) player).forEach(player -> {
            try {
                List<String> header = new ArrayList<>(headerText);
                List<String> footer = new ArrayList<>(footerText);
                if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
                    header = TextStatics.applyFunction(header, (line) -> PlaceholderAPI.setPlaceholders(player, line));
                    footer = TextStatics.applyFunction(footer, (line) -> PlaceholderAPI.setPlaceholders(player, line));
                }

                player.setPlayerListHeaderFooter(
                        TextStatics.mergeList(header, "\n"),
                        TextStatics.mergeList(footer, "\n"));
                player.setScoreboard(getSortingBoard());
            } catch (Exception x) {
                this.realmService.getLogger().error("Failed to update tab for '%s'!", x, player.getName());
            }
        });
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        LuckPerms api = this.realmService.getApiService().getLuckPermsApi().getApi();
        if (api != null) {
            User user = api.getUserManager().getUser(player.getUniqueId());
            if (user != null) {
                String groupName = user.getPrimaryGroup();
                Group group = api.getGroupManager().getGroup(groupName);
                if (group != null) {
                    AtomicInteger weight = new AtomicInteger(this.realmService.getPluginInstance().getConfig().getInt("services.tab-service.default-weight", 0));
                    group.getWeight().ifPresent(weight::set);

                    String priority = ScoreboardStatics.weightToAlpha(weight.get());
                    Team team = getSortingBoard().getTeam(priority + groupName);
                    if (team == null) {
                        this.realmService.getLogger().error("Can't find group %s for player %s", priority + groupName, player.getName());
                    }
                    else
                        team.addPlayer(player);
                }
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

    }


}

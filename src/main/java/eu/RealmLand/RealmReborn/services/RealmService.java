package eu.RealmLand.RealmReborn.services;

import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.api.APIService;
import eu.RealmLand.RealmReborn.services.chat.ChatService;
import eu.RealmLand.RealmReborn.services.i18n.LocalisationService;
import eu.RealmLand.RealmReborn.services.profile.ProfileService;
import eu.RealmLand.RealmReborn.services.storage.StorageService;
import eu.RealmLand.RealmReborn.services.tab.TabService;
import eu.RealmLand.RealmReborn.services.teleport.TeleportService;
import lombok.Getter;
import eu.RealmLand.RealmReborn.logger.Logger;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public class RealmService implements IService {

    @Getter
    @NotNull
    private Plugin pluginInstance;

    @Getter
    @NotNull
    private Logger logger;

    @Getter
    @NotNull
    private StorageService storageService;

    @Getter
    @NotNull
    private ProfileService profileService;

    @Getter
    @NotNull
    private APIService apiService;

    @Getter
    @NotNull
    private LocalisationService localisationService;

    @Getter
    @NotNull
    private CommandService commandService;

    @Getter
    @NotNull
    private ChatService chatService;

    @Getter
    @NotNull
    private TabService tabService;

    @Getter
    @NotNull
    private TeleportService teleportService;

    /**
     * Default constructor
     *
     * @param pluginInstance Instance of Plugin
     */
    public RealmService(@NotNull Plugin pluginInstance) {
        this.pluginInstance = pluginInstance;
        this.logger = new Logger();
        logger.info("Constructing RealmServices");

        try {
            this.apiService = new APIService(this);
            logger.info("Constructed '%s'", "APIService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "APIService");
            return;
        }

        try {
            this.storageService = new StorageService(this);
            logger.info("Constructed '%s'", "StorageService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "StorageService");
            return;
        }

        try {
            this.profileService = new ProfileService(this);
            logger.info("Constructed '%s'", "ProfileService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "ProfileService");
            return;
        }

        try {
            this.localisationService = new LocalisationService();
            logger.info("Constructed '%s'", "LocalisationService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "LocalisationService");
            return;
        }

        try {
            this.commandService = new CommandService(this);
            logger.info("Constructed '%s'", "CommandService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "CommandService");
            return;
        }

        try {
            this.chatService = new ChatService(this);
            logger.info("Constructed '%s'", "ChatService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "ChatService");
            return;
        }

        try {
            this.tabService = new TabService(this);
            logger.info("Constructed '%s'", "TabService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "TabService");
            return;
        }

        try {
            this.teleportService = new TeleportService(this);
            logger.info("Constructed '%s'", "TeleportService");
        } catch (Exception x) {
            logger.error("Failed to construct '%s'", x, "TeleportService");
            return;
        }
    }

    @Override
    public void initialize() throws Exception {
        try {
            this.apiService.initialize();
            logger.info("Initialized '%s'", "APIService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "APIService");
            return;
        }

        try {
            this.storageService.initialize();
            logger.info("Initialized '%s'", "StorageService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "StorageService");
            return;
        }

        try {
            this.profileService.initialize();
            logger.info("Initialized '%s'", "ProfileService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "ProfileService");
            return;
        }

        try {
            this.commandService.initialize();
            logger.info("Initialized '%s'", "CommandService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "CommandService");
            return;
        }

        try {
            this.chatService.initialize();
            logger.info("Initialized '%s'", "ChatService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "ChatService");
            return;
        }

        try {
            this.tabService.initialize();
            logger.info("Initialized '%s'", "TABService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "TABService");
            return;
        }

        try {
            this.teleportService.initialize();
            logger.info("Initialized '%s'", "TeleportService");
        } catch (Exception x) {
            logger.error("Failed to initialize '%s'", x, "TeleportService");
            return;
        }
    }

    @Override
    public void terminate() throws Exception {
        try {
            this.teleportService.terminate();
            logger.info("Terminated '%s'", "TeleportService");
        } catch (Exception x) {
            logger.error("Failed to terminate '%s'", x, "TeleportService");
            return;
        }

        this.chatService.terminate();

        this.tabService.terminate();

        try {
            this.commandService.terminate();
            logger.info("Terminated '%s'", "CommandService");
        } catch (Exception x) {
            logger.error("Failed to terminate '%s'", x, "CommandService");
            return;
        }
        try {
            this.profileService.terminate();
            logger.info("Terminated '%s'", "ProfileService");
        } catch (Exception x) {
            logger.error("Failed to terminate '%s'", x, "ProfileService");
            return;
        }

        this.storageService.terminate();
        this.apiService.terminate();
    }

    @Override
    public void reload() throws Exception {

    }


}

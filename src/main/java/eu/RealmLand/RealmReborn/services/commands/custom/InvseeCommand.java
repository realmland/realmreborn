package eu.RealmLand.RealmReborn.services.commands.custom;

import eu.RealmLand.RealmReborn.services.commands.CommandService;
import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InvseeCommand extends Command {

    @CommandParameters(name="invsee", permission = "realmland.admin.invsee")
    public InvseeCommand(@NotNull CommandService commandService, @NotNull String name) {
        super(commandService, name);
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String alias,  @NotNull String[] args, boolean isPlayer) throws Exception {
        if(args.length > 0) {
            String targetName = args[0];
            Player target = Bukkit.getPlayer(targetName);
            if(target == null || !target.isOnline()) {
                sender.sendMessage("§c# §fSpecify valid player");
                return true;
            }
            ((Player) sender).openInventory(target.getInventory());
        } else
            sender.sendMessage("§c# §fSpecify player");
        return true;
    }

    @Override
    public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        if(args.length == 1)
            return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        return new ArrayList<>();
    }
}

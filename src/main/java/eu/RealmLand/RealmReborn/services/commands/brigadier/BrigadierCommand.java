package eu.RealmLand.RealmReborn.services.commands.brigadier;


import com.google.common.base.Joiner;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.commands.brigadier.source.CommandSource;
import net.minecraft.server.v1_15_R1.CommandDispatcher;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.craftbukkit.v1_15_R1.CraftServer;
import org.bukkit.craftbukkit.v1_15_R1.command.VanillaCommandWrapper;
import org.jetbrains.annotations.NotNull;

public class BrigadierCommand extends BukkitCommand {

    @Getter
    @NotNull
    private CommandDispatcher nmsDispatcher;

    @Getter
    @NotNull
    private LiteralArgumentBuilder<CommandSource> command;


    public BrigadierCommand(@NotNull LiteralArgumentBuilder<CommandSource> command) {
        super(command.getLiteral());
        this.command = command;
        this.nmsDispatcher = ((CraftServer) Bukkit.getServer()).getHandle().getServer().commandDispatcher;
    }

    @Override
    public boolean execute(@NotNull CommandSender commandSender, @NotNull String label, @NotNull String[] arguments) {
        CommandSource source = (CommandSource) VanillaCommandWrapper.getListener(commandSender);
        nmsDispatcher.a(source, toDispatcher(arguments, getName()), toDispatcher(arguments, label));
        return true;

    }

    private static String toDispatcher(String[] args, String name) {
        return name + ((args.length > 0) ? " " + Joiner.on(' ').join(args) : "");
    }

}

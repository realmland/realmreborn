package eu.RealmLand.RealmReborn.services.commands.brigadier.source;

import com.mojang.brigadier.ResultConsumer;
import net.minecraft.server.v1_15_R1.*;

import javax.annotation.Nullable;

public class CommandSource extends CommandListenerWrapper {

    public CommandSource(ICommandListener icommandlistener, Vec3D vec3d, Vec2F vec2f, WorldServer worldserver, int i, String s, IChatBaseComponent ichatbasecomponent, MinecraftServer minecraftserver, @Nullable Entity entity) {
        super(icommandlistener, vec3d, vec2f, worldserver, i, s, ichatbasecomponent, minecraftserver, entity);
    }

    public CommandSource(ICommandListener icommandlistener, Vec3D vec3d, Vec2F vec2f, WorldServer worldserver, int i, String s, IChatBaseComponent ichatbasecomponent, MinecraftServer minecraftserver, @Nullable Entity entity, boolean flag, ResultConsumer<CommandListenerWrapper> resultconsumer, ArgumentAnchor.Anchor argumentanchor_anchor) {
        super(icommandlistener, vec3d, vec2f, worldserver, i, s, ichatbasecomponent, minecraftserver, entity, flag, resultconsumer, argumentanchor_anchor);
    }

}

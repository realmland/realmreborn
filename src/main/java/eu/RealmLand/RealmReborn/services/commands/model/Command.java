package eu.RealmLand.RealmReborn.services.commands.model;

import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import lombok.Getter;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public abstract class Command extends BukkitCommand {

    @Getter
    private CommandService commandService;

    public Command(@NotNull CommandService commandService, @NotNull String name) {
        super(name);
        this.commandService = commandService;
    }

    /**
     * Executes command
     *
     * @param sender   Sender
     * @param alias    Alias that has been called
     * @param args     Arguments of command
     * @param isPlayer Is player
     * @return Executed
     */
    public abstract boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception;

    @Override
    public final boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
        try {
            if(!sender.hasPermission(getPermission())) {
                sender.sendMessage("§c# §ffUh oh! Permissie nepasujú zlatý môj.");
                return true;
            }
            return execute(sender, alias, args, sender instanceof Player);
        } catch (Exception x) {
            sender.sendMessage("§c# §fInternal exception occurred while executing this command.");
            this.commandService
                    .getRealmService()
                    .getLogger()
                    .error("Error occurred during execution of '%s'", x, alias);
            return true;
        }
    }

    public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) {
        return new ArrayList<>();
    }


    @Override
    public final @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) throws IllegalArgumentException {
        List<String> completions = executeTabComplete(sender, alias, args, sender instanceof Player);

        return completions;
    }

    @Override
    public final @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias,  @NotNull String[] args, @Nullable Location location) throws IllegalArgumentException {
        return super.tabComplete(sender, alias, args, location);
    }

    protected Profile requireOnlyPlayer(@NotNull CommandSender sender) {
        if(sender instanceof Player)
            return this.commandService.getRealmService().getProfileService().getProfile(((Player) sender).getUniqueId());
        else
            return null;
    }
}

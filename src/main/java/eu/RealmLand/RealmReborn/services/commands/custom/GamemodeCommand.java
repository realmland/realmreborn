package eu.RealmLand.RealmReborn.services.commands.custom;

import eu.RealmLand.RealmReborn.services.commands.CommandService;
import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import eu.RealmLand.RealmReborn.services.profile.model.Profile;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GamemodeCommand extends Command {

    @CommandParameters(name = "gamemode", permission = "realmland.admin.gamemode", aliases = {"gm", "gmc", "gms", "gma", "gmsp"})
    public GamemodeCommand(@NotNull CommandService commandService, @NotNull String name) {
        super(commandService, name);
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
        GameMode mode = null;

        if (alias.equalsIgnoreCase("gmc")) mode = GameMode.CREATIVE;
        else if (alias.equalsIgnoreCase("gms")) mode = GameMode.SURVIVAL;
        else if (alias.equalsIgnoreCase("gma")) mode = GameMode.ADVENTURE;
        else if (alias.equalsIgnoreCase("gmsp")) mode = GameMode.SPECTATOR;


        if (args.length > 0) {
            // read from arguments if already not specified
            if (mode == null)
                try {
                    String modeName = args[0].toUpperCase();
                    args = (String[]) ArrayUtils.remove(args, 0);

                    // maybe its shortcut? try
                    switch (modeName) {
                        case "C":
                        case "1":
                        case "CREATIVE":
                            mode = GameMode.CREATIVE;
                            break;
                        case "A":
                        case "2":
                        case "ADVENTURE":
                            mode = GameMode.ADVENTURE;
                            break;
                        case "S":
                        case "0":
                        case "SURVIVAL":
                            mode = GameMode.SURVIVAL;
                            break;
                        case "SP":
                        case "3":
                        case "SPECTATOR":
                        case "SPEC":
                            mode = GameMode.SPECTATOR;
                            break;
                    }

                    // well what if not
                    if (mode == null)
                        mode = GameMode.valueOf(modeName);

                } catch (IllegalArgumentException x) {
                    sender.sendMessage("§c# §fUnknown Gamemode '" + args[0] + "'");
                    return true;
                }
        }

        if (mode == null) {
            sender.sendMessage("§c# §fYou must specify gamemode");
            return true;

        }

        if(args.length > 0) {
            String targetName = args[0];
            Player target = Bukkit.getPlayer(targetName);

            if(!sender.hasPermission("realmland.admin.gamemode.others")) {
                sender.sendMessage("§c# §fYou don't have enough permissions.");
                return true;
            }

            if(target != null) {

                if (target.getGameMode().equals(mode)) {
                    sender.sendMessage("§c# §fHe already is in this gamemode! Chumaj:D");
                    return true;
                }

                this.getCommandService().getRealmService().getLogger().info("Player '" + sender.getName() + "' has changed " + targetName + "'s gamemode to '§e" + mode.name().toLowerCase() + "§f' from '§e" + target.getGameMode().name().toLowerCase() + "§f'");
                target.setGameMode(mode);
                sender.sendMessage("§a# §f Gamemode of §e" + targetName + "§r changed to '§e" + mode.name().toLowerCase() + "§r'");
                return true;
            } else {
                sender.sendMessage("§c# §fInvalid player!");
                return true;
            }
        }

        Profile profile = super.requireOnlyPlayer(sender);
        if(profile == null)
            return true;

        Player p = (Player) sender;

        if (p.getGameMode().equals(mode)) {
            sender.sendMessage("§c# §fYou already are in this gamemode! Chumaj:D");
            return true;
        }



        this.getCommandService().getRealmService().getLogger().info("Player '§e" + p.getName() + "§r' has changed his gamemode to '§e" + mode.name().toLowerCase() + "§f' from '§e" + p.getGameMode().name().toLowerCase() + "§f'");
        p.setGameMode(mode);
        sender.sendMessage("§a# §fGamemode changed to '§e" + mode.name().toLowerCase() + "§f'");


        return true;
    }


    @Override
    public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws IllegalArgumentException {
        if (args.length == 1) {
            if(alias.equalsIgnoreCase("gmc") || alias.equalsIgnoreCase("gms") || alias.equalsIgnoreCase("gma") || alias.equalsIgnoreCase("gmsp")) {
                return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
            }
            return Arrays.asList("creative", "survival", "adventure", "spectator");
        }
        if(args.length == 2)
            return Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList());
        return new ArrayList<>();
    }
}

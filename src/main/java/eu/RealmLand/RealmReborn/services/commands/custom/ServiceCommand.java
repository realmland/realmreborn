package eu.RealmLand.RealmReborn.services.commands.custom;

import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import eu.RealmLand.RealmReborn.services.commands.CommandService;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceCommand extends Command {

    @CommandParameters(name = "realmservices", aliases = {"services", "rs"}, permission = "realmland.admin.services")
    public ServiceCommand(@NotNull CommandService commandService, @NotNull String name) {
        super(commandService, name);
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws Exception {
        if(args.length > 0) {
            String request = args[0];
            if(request.equalsIgnoreCase("reload")) {
                if(args.length > 1) {
                    String serviceName = args[1];
                    if(serviceName.equalsIgnoreCase("tab")) {
                        this.getCommandService().getRealmService().getTabService().reload();
                        sender.sendMessage("§a# §fReloaded tab");
                    }
                    if(serviceName.equalsIgnoreCase("storage")) {
                        this.getCommandService().getRealmService().getStorageService().reload();
                        this.getCommandService().getRealmService().getPluginInstance().reloadConfig();
                        sender.sendMessage("§a# §fReloaded storage");
                    }
                } else
                    sender.sendMessage("service name required");
            }
        }
        return true;
    }

    @Override
    public @NotNull List<String> executeTabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args, boolean isPlayer) throws IllegalArgumentException {
        if(args.length == 1)
            return Arrays.asList("reload");
        if(args.length == 2)
            return Arrays.asList("tab", "storage");
        return new ArrayList<>();
    }
}

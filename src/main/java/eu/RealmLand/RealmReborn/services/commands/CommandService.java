package eu.RealmLand.RealmReborn.services.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import eu.RealmLand.RealmReborn.IService;
import eu.RealmLand.RealmReborn.services.RealmService;
import eu.RealmLand.RealmReborn.services.commands.model.Command;
import eu.RealmLand.RealmReborn.services.commands.model.CommandParameters;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_15_R1.CommandDispatcher;
import net.minecraft.server.v1_15_R1.CommandListenerWrapper;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_15_R1.CraftServer;
import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class CommandService implements IService {

    @Getter
    @NotNull
    private RealmService realmService;

    @Getter
    @NotNull
    private CommandDispatcher nmsDispatcher;

    @Getter
    @Setter
    private boolean debugSearch = false;

    /**
     * Default constructor
     *
     * @param realmService Instance of realm service
     */
    public CommandService(@NotNull RealmService realmService) {
        this.realmService = realmService;
        nmsDispatcher = ((CraftServer) Bukkit.getServer()).getHandle().getServer().commandDispatcher;

        LiteralArgumentBuilder<CommandListenerWrapper> command = CommandDispatcher.a("brigadier").executes(cmd -> {
            System.out.println("it works");
            return 1;
        });

        ((CraftServer) Bukkit.getServer()).getHandle().getServer().commandDispatcher.a().register(command);
    }

    @Override
    public void initialize() throws Exception {
        try {
            new JarFile(new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI())).stream()
                    .filter(entry -> !entry.isDirectory())
                    .map(JarEntry::getName)
                    // only class files
                    .filter(entry -> entry.endsWith(".class"))
                    .map(entry -> entry.replaceAll("[/]", "."))
                    .map(entry -> entry.substring(0, entry.length() - ".class".length()))
                    .map(entry -> {
                        try {
                            return Class.forName(entry);
                        } catch (Exception x) {
                            if (debugSearch)
                                this.getRealmService().getLogger().error("Didnt find class for entry '%s'", entry);
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .filter(Command.class::isAssignableFrom)
                    .map(clazz -> {
                        try {
                            return clazz.getConstructor(CommandService.class, String.class);
                        } catch (NoSuchMethodException ignored) {
                            if (debugSearch)
                                this.getRealmService().getLogger().error("Didnt find constructor in command class '%s'", clazz.getSimpleName());
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .forEach(constructor -> {
                        CommandParameters parameters = constructor.getAnnotation(CommandParameters.class);
                        try {
                            Command command = ((Command) constructor.newInstance(this, parameters.name()));
                            command.setPermission(parameters.permission());
                            if (Bukkit.getPluginManager().getPermission(parameters.permission()) == null) {
                                Bukkit.getPluginManager().addPermission(new Permission(parameters.permission(), "Realm permission", parameters.permissionDefault()));
                                getRealmService().getLogger().debug("Registered permission '%s' from command '%s' (%s)",
                                        parameters.permission(),
                                        command.getName(),
                                        parameters.permissionDefault());
                            }
                            command.setAliases(Arrays.asList(parameters.aliases()));
                            ((CraftServer) Bukkit.getServer()).getCommandMap().register(parameters.name(), "realmland", command);
                        } catch (InstantiationException e) {
                            if (debugSearch) e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            if (debugSearch)
                                e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            if (debugSearch)
                                e.printStackTrace();
                        } catch (NullPointerException x) {
                            if (debugSearch)
                                this.getRealmService().getLogger().error("NullPtr for '%s'", constructor.getName());
                        }
                    });

        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    @Override
    public void terminate() throws Exception {

    }

    @Override
    public void reload() throws Exception {

    }
}

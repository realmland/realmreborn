package eu.RealmLand.RealmReborn.services.commands.model;

import org.bukkit.permissions.PermissionDefault;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CommandParameters {

    String name();
    String[] aliases() default {};
    String permission() default "realmland.admin";
    PermissionDefault permissionDefault() default PermissionDefault.OP;

}

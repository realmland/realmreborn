package eu.RealmLand.RealmReborn.statics;

import org.bukkit.ChatColor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextStatics {

    /**
     * Merges String list into one String
     * @param origin    List of Strings
     * @param delimiter Delimiter
     * @return Merged string
     */
    public static String mergeList(@NotNull List<String> origin, @NotNull String delimiter) {
        StringBuilder merged = new StringBuilder();
        origin.forEach(line -> merged.append(line).append(delimiter));

        return merged.toString();
    }

    /**
     * Applies function to every single entry of List
     * @param origin   List
     * @param function Function to be applies
     * @return New List
     */
    public static List<String> applyFunction(@NotNull List<String> origin, Function<String, String> function) {
        return origin.stream().map(function).collect(Collectors.toList());
    }

    /**
     * Applies colors to string
     * @param string String
     * @param colors Colors
     * @return Colored String
     */
    public static String applyColors(@NotNull String string, @NotNull List<ChatColor> colors) {
        String rawColors = colors.stream().map(ChatColor::toString).collect(Collectors.joining());
        return rawColors + string;
    }

}

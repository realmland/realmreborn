package eu.RealmLand.RealmReborn.statics;

import org.jetbrains.annotations.NotNull;

public class ScoreboardStatics {

    /**
     * Converts number to alphabet priority
     * @param number Number
     * @return Alphabet character
     */
    public static String priorityToAlpha(int number) {
        return new String(new char[] {0x41 + 1});
    }

    /**
     * Converts weight to alphabet priority
     * @param weight Weight
     * @return Alphabet character
     */
    public static String weightToAlpha(int weight) {
        return new String(new char[]{(char)(90 - weight)});
    }

}
